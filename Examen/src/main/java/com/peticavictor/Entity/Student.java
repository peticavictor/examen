package com.peticavictor.Entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "student")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_student")
    private int studentId;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "birth_date")
    private Date  birthDate;

    @Column(name = "gender")
    private char gender;

    @Column(name = "adress")
    private String address;

    @Column(name = "grup")
    private String group;

    @Column(name = "faculty")
    private String faculty;

    @Column(name = "java")
    private double java;

    @Column(name = "cpp")
    private double cpp;

    @Column(name = "db")
    private double db;

    @Column(name = "csharp")
    private double csharp;

    public Student() {
    }

    public Student(int studentId, String firstName, String lastName, Date birthDate,
                   char gender, String address, String group, String faculty,
                   double java, double cpp, double db, double csharp) {
        this.studentId = studentId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.group = group;
        this.faculty = faculty;
        this.java = java;
        this.cpp = cpp;
        this.db = db;
        this.csharp = csharp;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public double getJava() {
        return java;
    }

    public void setJava(double java) {
        this.java = java;
    }

    public double getCpp() {
        return cpp;
    }

    public void setCpp(double cpp) {
        this.cpp = cpp;
    }

    public double getDb() {
        return db;
    }

    public void setDb(double db) {
        this.db = db;
    }

    public double getCsharp() {
        return csharp;
    }

    public void setCsharp(double csharp) {
        this.csharp = csharp;
    }

    @Override
    public String toString() {
        return "Student{" +
                "studentId=" + studentId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthDate=" + birthDate +
                ", gender=" + gender +
                ", address='" + address + '\'' +
                ", group='" + group + '\'' +
                ", faculty='" + faculty + '\'' +
                ", java=" + java +
                ", cpp=" + cpp +
                ", db=" + db +
                ", csharp=" + csharp +
                '}';
    }
}
