package com.peticavictor.DAO;

import com.peticavictor.Entity.Student;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class StudentDAOImpl implements StudentDAO{
    private final SessionFactory sessionFactory;

    public StudentDAOImpl() {

//        Creating session factory object (hibernate)
        sessionFactory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Student.class)
                .buildSessionFactory();
    }

//    method for saving student
    @Override
    public void saveStudent(Student student) {

        try{
//           getting session from session factory
            Session session = sessionFactory.getCurrentSession();
            session.beginTransaction();

//           saving student in db
            session.save(student);

//           closing transaction
            session.getTransaction().commit();

//           output inserted student
            System.out.println("Student " + student + " added");
        } catch (HibernateException e) {
            e.printStackTrace();
        }
    }


    @Override
    public List<Student> getAllStudents() {
        List<Student> students = null;

        try {
//       getting session
            Session session = sessionFactory.getCurrentSession();

//       begin transaction
            session.beginTransaction();

//       getting List of all students

            students = session.createQuery("from Student", Student.class).getResultList();

//       commit transaction
            session.getTransaction().commit();
        } catch (HibernateException e) {
            e.printStackTrace();
        }

        return students;
    }

    @Override
    public List<Student> getStudentsFromGroup(String group) {
        List<Student> students = null;

        try {
//       getting session
            Session session = sessionFactory.getCurrentSession();

//       begin transaction
            session.beginTransaction();

//       getting List students from specific group with setting parameter
            students = session.createQuery("from Student where group = :group", Student.class)
                    .setParameter("group",group)
                    .getResultList();

//       commit transaction
            session.getTransaction().commit();

        } catch (HibernateException e) {
            e.printStackTrace();
        }

        return students;
    }

//    method get student by id
    @Override
    public Student getStudent(int id) {

        Student student = null;
        try {
//       getting session
            Session session = sessionFactory.getCurrentSession();

//       begin transaction
            session.beginTransaction();

//       getting student by id
            student = session.get(Student.class,id);

//       commit transaction
            session.getTransaction().commit();
        } catch (HibernateException e) {
            e.printStackTrace();
        }

        return student;
    }

//    method save student changes ( for notes )
    @Override
    public void setNote( Student student) {
        try {
//       getting session
            Session session = sessionFactory.getCurrentSession();

//       begin transaction
            session.beginTransaction();

//       updating student's note
            session.update(student);

//       commit transaction
            session.getTransaction().commit();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
    }
}
