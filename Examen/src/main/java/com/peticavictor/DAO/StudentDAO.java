package com.peticavictor.DAO;

import com.peticavictor.Entity.Student;

import java.util.List;

public interface StudentDAO {
    void saveStudent(Student student);
    List<Student> getAllStudents();
    List<Student> getStudentsFromGroup(String group);
    Student getStudent(int id);
    void setNote( Student student);
}
