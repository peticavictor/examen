package com.peticavictor;

import com.peticavictor.Entity.Student;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;

public class Methods {
    private static Scanner scanner = new Scanner(System.in);

    public static Student insertStudent() {
        Student student = new Student();
        //ask user to input student group
        System.out.println("Insert Student Group :");
        student.setGroup(scanner.next());

        //ask user to input student name
        System.out.println("Insert Student Name :");
        student.setFirstName(scanner.next());

        //ask user to input student surname
        System.out.println("Insert Student Surname :");
        student.setLastName(scanner.next());

        //ask user to input student gender
        System.out.println("Insert Student Gender :");
        student.setGender(scanner.next().charAt(0));

        //ask user to input student birthdate
        System.out.println("Insert Student Birthdate :");
        try {
            student.setBirthDate(new SimpleDateFormat("dd/MM/yyyy").parse(scanner.next()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //ask user to input student address
        System.out.println("Insert Student address :");
        student.setAddress(scanner.next());

        //ask user to input student faculty
        System.out.println("Insert Student faculty :");
        student.setFaculty(scanner.next());

        return student;
    }
}
