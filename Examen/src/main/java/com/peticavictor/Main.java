package com.peticavictor;

import com.peticavictor.DAO.StudentDAO;
import com.peticavictor.DAO.StudentDAOImpl;
import com.peticavictor.Entity.Student;

import java.util.Scanner;

public class Main {

    private final static StudentDAO studentDAO = new StudentDAOImpl();

    public static void main(String[] args) {

        //creating object scanner
        Scanner scanner = new Scanner(System.in);

        while (true) {

//             ask user to choose an option
            System.out.println("1. add new student");
            System.out.println("2. show all students");
            System.out.println("3. show students of a group");
            System.out.println("4. noting students");
            System.out.println("5. exit");
            
//            switch to option selected by user
            switch (scanner.next()) {
                case "1" -> {

//                    creating new student and input fields
//                    saving student
                    studentDAO.saveStudent(Methods.insertStudent());
                }
                case "2" -> {
                    System.out.println("All Students : ");

//                    output All students
                    for (Student student1 : studentDAO.getAllStudents()) {
                        System.out.println(student1);
                    }
                    System.out.println();
                }
                case "3" -> {
//                    ask user to insert group name
                    System.out.println("Insert name of the group");
                    String group = scanner.next();

//                    output students from group
                    for (Student student1 : studentDAO.getStudentsFromGroup(group)) {
                        System.out.println(student1);
                    }
                }
                case "4" -> {
//                    ask user to insert a group
                    System.out.println("Insert group : ");

//                    printing students from group
                    for (Student student1 : studentDAO.getStudentsFromGroup(scanner.next())) {
                        System.out.println(student1);
                    }

//                    ask user to choose a student by id
                    System.out.println("Choose student by id : ");

//                    getting and printing student
                    Student student = studentDAO.getStudent(scanner.nextInt());
                    System.out.println(student);

                    while (true) {
//                        ask user to choose class
                        System.out.println("Choose class : ");
                        System.out.println("1 : Java");
                        System.out.println("2 : CPP");
                        System.out.println("3 : DB");
                        System.out.println("4 : CSharp");

                        String className = scanner.next();

//                        ask user to input note
                        System.out.println("Input note : ");
                        int note = scanner.nextInt();

//                        setting student's note depending on chosen class
                        switch (className) {
//                            case for class java
                            case "1" :
                                student.setJava(student.getJava() > 0 ? (student.getJava() + note) / 2 : note );
//                                case for cpp
                            case "2" :
                                student.setCpp(student.getCpp() > 0 ? (student.getCpp() + note) / 2 : note );
//                                case for DB
                            case "3" :
                                student.setDb(student.getDb() > 0 ? (student.getDb() + note) / 2 : note );
//                                case for Csharp
                            case "4" :
                                student.setCsharp(student.getCsharp() > 0 ? (student.getCsharp() + note) / 2 : note );
                        }
                        System.out.println(student);

//                        updating student
                        studentDAO.setNote(student);

//                        ask user to choose continue or back
                        System.out.println("1 - continue : 0 - back");
                        if(!scanner.next().equals("1")) {
                            break;
                        }
                    }
                }
                case "5" -> {
//                    exit
                    System.out.println("Application terminate!");
                    System.exit(0);
                }
            }
        }
    }
}
